import React, { Component } from 'react';
import {
  CheckBox,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';

export default class LoginView extends Component {

  constructor(props) {
    super(props);
    state = {
      username   : '',
      password: '',
    }
  }

  onClickListener = (viewId) => {
    Alert.alert("Alert", "Button pressed "+viewId);
  }

  render() {
    return (
      <View style={styles.container}>
            {<Image style={styles.inputIcon} source={{uri: 'https://freepngimg.com/thumb/wordpress_logo/5-2-wordpress-logo-free-download-png.png' }}/>}
        <View style={styles.view2}>
          <TouchableHighlight style={styles.lableText}>
            <Text>Username</Text>
          </TouchableHighlight>
          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                keyboardType="default"
                underlineColorAndroid='transparent'
                onChangeText={(username) => this.setState({username})}/>
          </View>
          
          <TouchableHighlight style={styles.lableText}>
            <Text>Password</Text>
          </TouchableHighlight>
          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                secureTextEntry={true}
                underlineColorAndroid='transparent'
                onChangeText={(password) => this.setState({password})}/>
          </View>

          <CheckBox style={styles.checkBox} title='Remember Me'/>
          <TouchableHighlight style={styles.remember}>
            <Text>Remember Me</Text>
          </TouchableHighlight>
          
          <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.onClickListener('login')}>
            <Text style={styles.loginText}>Log In</Text>
          </TouchableHighlight>
        </View>

        <TouchableHighlight style={styles.buttonContainer} onPress={() => this.onClickListener('restore_password')}>
            <Text>Register | Lost your password?</Text>
        </TouchableHighlight>

        <TouchableHighlight style={styles.buttonContainer} onPress={() => this.onClickListener('register')}>
            <Text>Back to Codex Sample</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
  },
  inputContainer: {
      borderBottomColor: '#898d90',
      backgroundColor: '#d2d6da',
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#898d90',
      flex:1,
  },
  inputIcon:{
    width:130,
    height:130,
    marginLeft:15,
    marginBottom:20,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:5,
  },
  loginButton: {
    backgroundColor: "#4285f4",
    width:80,
    marginTop:-30,
    marginLeft:170,
  },
  loginText: {
    color: 'white',
  },
  lableText:{
    marginBottom: 5,
    right: 93
  },
  view2:{
      height:280,
      alignItems: 'center',
      backgroundColor: '#FFFFFF',
      width: 300,
  },
  remember:{
    marginTop:-30,
    marginBottom: 5,
    right:50
  },
  checkBox:{
    right:115,
    marginBottom: 5,
  }
  
});